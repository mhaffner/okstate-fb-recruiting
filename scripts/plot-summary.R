library(sp)
library(sf)
library(udunits2)

# set working directory within the repository
setwd("/home/matt/git-repos/okstate-fb-recruiting/data/")

# load data
recruits <- data.frame(read.csv("osu-recruits.csv"))
years <- unique(recruits$year)

multipoints <- st_multipoint(as.matrix(recruits[,c("lon", "lat")]))
points <- st_cast(st_geometry(multipoints), "POINT")

# create point for just stillwater
stillwater <- st_geometry(st_point(c(-97.0665, 36.1257)))
st_crs(stillwater)  <- "+init=epsg:4326"


for (k in years) {
  multipoints <- st_multipoint(as.matrix(recruits[,c("lon", "lat")][recruits$year == k,]))
  points <- st_cast(st_geometry(multipoints), "POINT")
  st_crs(points)  <- "+init=epsg:4326"

  for (i in 1:length(points)) {
    # combine each point with stillwater to make a pair of points
    pair <- st_combine(c(points[i], stillwater))
    dist <- st_distance(points[i], stillwater) %>%
      # convert from meters to miles
      ud.convert(., "m", "mi")

    # create a line from this pair of points
    line <- st_cast(pair, "LINESTRING")

    # combine lines into multilinestring for plotting and combine distances
    # together for computations
    if (i == 1) {
      lines <- line
      distances <- dist
    } else {
      lines <- st_combine(c(lines, line))
      distances <- append(distances, dist)
    }
  }

  ### distances ###
  # append to dataframe
  if (k == min(years)) {
    dist.df <- data.frame(distance = as.numeric(distances), year = k)
  } else {
    tmp.df <- data.frame(distance = as.numeric(distances), year = k)
    dist.df <- rbind(dist.df, tmp.df)
  }

  ### lines ###
  # covert to sp object
  lines.sldf <- sf:::as_Spatial(lines)

  # assign year to column
  lines.sldf$year <- k

  if (k == min(years)) {
    lines.all <- lines.sldf
  } else {
    lines.all <- rbind(lines.all, lines.sldf)
  }
}

# create vectors of means, medians, and standard deviation by year
mean.dist <- tapply(dist.df$distance, dist.df$year, mean)
median.dist <- tapply(dist.df$distance, dist.df$year, median)
sd.dist <- tapply(dist.df$distance, dist.df$year, sd)

# create data frame of these three vectors
summary.df <- data.frame(mean = mean.dist,
                         median = median.dist,
                         sd = sd.dist)

png("../img/summary.png",
    width = 800,
    height = 800,
    units = "px",
    res = 110)
plot.new()
par(mfrow=c(3,1))
par(mar = c(4,4,4,4))

# plot for mean
plot(summary.df$mean,
     type = "l",
     xlab = "",
     ylab = "Distance (miles)",
     xaxt = "n", # remove xaxis labels; add them in the next line
     xaxs = "i") # remove excess space in the plot
title("Mean distance from Stillwater")
labs <- years
axis(side=1, labels=labs, at=c(1:length(years)))

# plot for median
plot(summary.df$median,
     type = "l",
     xlab = "",
     ylab = "Distance (miles)",
     xaxt = "n", # remove xaxis labels; add them in the next line
     xaxs = "i") # remove excess space in the plot
title("Median distance from Stillwater")
labs <- years
axis(side=1, labels=labs, at=c(1:length(years)))

# plot for standard deviation
plot(summary.df$sd,
     type = "l",
     xlab = "",
     ylab = "Distance (miles)",
     xaxt = "n", # remove xaxis labels; add them in the next line
     xaxs = "i") # remove excess space in the plot
title("Standard deviation of distance from Stillwater")
labs <- years
axis(side=1, labels=labs, at=c(1:length(years)))
dev.off()
